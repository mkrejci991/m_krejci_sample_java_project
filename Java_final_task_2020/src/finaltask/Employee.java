package finaltask;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Employee implements Serializable {
	private String firstName;
	private String lastName;
	private String dateOfBirth;
	private String gender;
	private String phoneNumber;
	private String position;
	private Department department;
	private Employee superior;
	private String dateOfEmployment;
	private int yearOfEmployment;
	private int monthOfEmployment;
	private int dayOfEmployment;
	private int salary;
	
	public Employee	(
			String firstName,String lastName, String dateOfBirth, String gender,
			String phoneNumber, String position, Department department,
			int yearOfEmployment, int monthOfEmployment, int dayOfEmployment, int salary
	) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.phoneNumber= phoneNumber;
		this.position = position;
		this.department = department;
		
		this.yearOfEmployment = yearOfEmployment;
		this.monthOfEmployment = monthOfEmployment;
		this.dayOfEmployment = dayOfEmployment;
		
		this.dateOfEmployment = yearOfEmployment + "-" + monthOfEmployment + "-" + dayOfEmployment;
		this.salary = salary;
		
	}
	
	//getters and setters
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	//combines methods to get the full name of an employee
	public String getFullName()	{
		return String.format("%s %s", firstName, lastName);
	}
	
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getPosition() {
		return position;
	}
	
	public void setPosition(String position) {
		this.position = position;
	}
	
	
	public Department getDepartment() {
		return department;
	}
	
	public void setDepartment(Department department) {
		this.department = department;
	}
	
	public Employee getSuperior() {
		return superior;
	}
	
	public void setSuperior(Employee superior) {
		this.superior = superior;
	}
	
	public String getDateOfEmployment() {
		return dateOfEmployment;
	}
	
	public int getYearOfEmployment() {
		return yearOfEmployment;
	}
	
	public int getMonthOfEmployment() {
		return monthOfEmployment;
	}
	
	public int getDayOfEmployment() {
		return dayOfEmployment;
	}
	
	public void setDateOfEmployment(String dateOfEmployment) {
		this.dateOfEmployment  = dateOfEmployment;
	}
	
	public int getSalary() {
		return salary;
	}
	
	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	@Override
	public String toString() {
		String str = "";
		
		str += "Name: "+getFullName()+"\n";
		str += "Date of birth: "+ dateOfBirth + "\n";
		str += "Gender: "+ gender + "\n";
		str += "Phone number: "+ phoneNumber + "\n";
		str += "Position: " + position + "\n";
		str += "Department: " + department + "\n";
		str +="Superior: ";
		if(getSuperior() != null) {
			str += getSuperior().getFullName();
		} else {
			str += "-";
		}
		str+="\n";
		str += "Date of Employment: "+ dateOfEmployment + "\n";
		str += "Salary: "+ salary + "\n";
		
		return str;
	}
	
}
