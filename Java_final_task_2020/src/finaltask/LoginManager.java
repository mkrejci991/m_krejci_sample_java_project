package finaltask;

import java.io.*;
import java.util.*;


@SuppressWarnings("serial")
public class LoginManager implements Serializable {
	private String login;
	private String password;
	private File file;
	@SuppressWarnings("unused")
	private boolean loggedIn;
	
	public LoginManager(String login, String password) {
		this.login = login;
		this.password = password;
		loggedIn = false;
		
	}
	
	public String getLogin() {
		return login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public File getFile() {
		return file;
	}
	
	
	public static void initiate()
	{
		String fullName = System.getProperty("user.dir")+File.separator+"LoginData.txt";
		
		//sets the name of the file
		File file = new File(fullName);
		
		// checks whether this file exists, creates a file if not
		if(!file.exists()) {
			LoginManager newMng = createNewMng();
			
			try	{
				if(file.createNewFile()) {
					try(	FileOutputStream fos = new FileOutputStream(file);
							ObjectOutputStream oos = new ObjectOutputStream(fos) ) {
						oos.writeObject(newMng);
						activateMng(newMng);
					}
					catch(Exception e) {
						System.err.println("An exception occured!");
						return;
					}
					
				} else {
					System.out.println("Something went wrong!");
				}
			}
			catch(IOException ex) {
				System.err.println("An exception occured!");
				ex.printStackTrace();
				return;
			}
		}
		//when the manager has been created
		else {
			
			try(FileInputStream fis = new FileInputStream(file);
					ObjectInputStream ois = new ObjectInputStream(fis))	{
				LoginManager mng = (LoginManager) ois.readObject();
				activateMng(mng);
			}
			catch(Exception ex)	{
				System.err.println("An exception occured while launching the Login manager program.");
				ex.printStackTrace();
				return;
			}
		}
		//AccountingSoftware acSW = new AccountingSoftware();
		//acSW.runAccSoftware();
	}
	
	@SuppressWarnings("resource")
	private static LoginManager createNewMng() {
		Scanner scan = new Scanner(System.in);
		LoginManager newMng = null;
		
		while(true)	{
			System.out.print("Enter the user name: ");
			String login = scan.nextLine();
			System.out.print("Enter password: ");
			String pass1 = scan.nextLine();
			System.out.print("Confirm password: ");
			String pass2 = scan.nextLine();
			
			if(pass1.equals(pass2))	{
				newMng = new LoginManager(login, pass1);
				System.out.println("Login account successfully created!");
				break;
			} else {
				System.out.println("Password not confirmed, please try again.");
			}
		}
		
		return newMng;
	}
	
	//sets the manager as active
	private static void activateMng(LoginManager mng) {
		Scanner scan = new Scanner(System.in);
		
		// number of failed attempts to log in before the program closes
		int wrongCount = 4;
		
		while(wrongCount >= 0) {
			if(wrongCount == 0)	{
				System.out.println("Failed to repeatedly log in to the program, closing now...");
				scan.close();
				break;
			}
			
			System.out.print("Enter login: ");
			String login = scan.nextLine();
			System.out.print("Enter password: ");
			String password = scan.nextLine();
			
			if((login.equals(mng.getLogin())) && (password.equals(mng.getPassword()))) {
				mng.loggedIn = true;
				System.out.println("Successfully logged in!");
				AccountingSoftware acSW = new AccountingSoftware(scan);
				acSW.runAccSoftware();
				mng.closeMng();
				scan.close();
				break;
			} else {
				wrongCount--;
				if(wrongCount>0) {
					System.out.printf("Invalid login or password, try again (%d)\n", wrongCount);
				}
			}
		}
	}
	
	//closes an individual Login Manager
	private void closeMng()	{
		loggedIn = false;
	}
}
