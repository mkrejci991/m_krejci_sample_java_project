package finaltask;

import java.io.*;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Department implements Serializable {
	private String name;
	private ArrayList<Employee> employees;
	private Employee headOfDpt;
	
	public Department(String name) {
		name = name.replace("department", "").replace("Department", "");
		this.name = name;
		employees = new ArrayList<Employee>();
	}
	
	public ArrayList<Employee> getEmployees() {
		return employees;
	}
	
	//average salary by department
	public double calculateAverage() {
		double avg = 0;
		for (Employee e : employees) {
			avg += e.getSalary();
		}
		
		avg /= (double)employees.size();
		return avg;
	}
	
	//getters and setters
	public Employee getHeadOfDpt() {
		return headOfDpt;
	}
	
	public void setHeadOfDpt(Employee headOfDpt) {
		this.headOfDpt = headOfDpt;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return name + " department";
	}
	
}
