package finaltask;

import java.util.Scanner;

public class EmployeeManager {
	private AccountingSoftware acsw;
	private Scanner scan;
	
	public EmployeeManager(AccountingSoftware acsw, Scanner scan) {
		this.acsw = acsw;
		this.scan = scan;
	}
	
		
	public void addEmployee() {
		if(acsw.getDepartments().size() == 0) {
			System.out.println("There are currently no departments...");
			System.out.print("Enter the name of the first department: ");
			String dptName = scan.nextLine();
			acsw.getDepartments().add(new Department(dptName));
			System.out.println(dptName + " created.\n");
		}
		//name and surname of the new employee		
		System.out.print("Enter first name: ");
		String firstName = scan.nextLine();
		System.out.print("Enter last name: ");
		String lastName = scan.nextLine();
		//the new employee's birth date
		System.out.print("Enter year of birth: ");
		String yearOfBirth = scan.nextLine();
		System.out.print("Enter month of birth: ");
		String monthOfBirth = scan.nextLine();
		System.out.print("Enter day of birth: ");
		String dayOfBirth = scan.nextLine();
		String dateOfBirth = yearOfBirth + "-" + monthOfBirth + "-" + dayOfBirth;
		//gender
		System.out.println("Choose gender: ");
		System.out.println("1 - male ");
		System.out.println("2 - female ");
		System.out.print("Enter choice: ");
		String gender = scan.nextLine();
		switch(gender) {
			case "1":
				gender = "male";
				break;
			case "2":
				gender = "female";
				break;
			default:
				System.out.println("Invalid choice");
				break;
		}
		System.out.print("Enter telephone number: ");
		String phoneNumber = scan.nextLine();
		System.out.print("Enter position: ");
		String position = scan.nextLine().trim();
		System.out.println();
		System.out.println("Select department: ");
		Department department = null;
		Department[] dpts = new Department[acsw.getDepartments().size()];
		dpts = acsw.getDepartments().toArray(dpts);
			for(int i = 0; i < dpts.length; i++) {
				System.out.println(dpts[i].getName() + " - " + (i + 1));
			}
		System.out.print("Enter selection: ");
		int selectDpt = Integer.parseInt(scan.nextLine());
		department = dpts[selectDpt-1];
		
		System.out.print("Enter year of employment: ");
		int yearOfEmployment = Integer.parseInt(scan.nextLine());
		System.out.print("Enter month of employment: ");
		int monthOfEmployment = Integer.parseInt(scan.nextLine());
		System.out.print("Enter day of employment: ");
		int dayOfEmployment = Integer.parseInt(scan.nextLine());
		System.out.print("Enter the employee's salary: ");
		int salary = Integer.parseInt(scan.nextLine());
		
		Employee newEmp = new Employee (
					firstName, lastName, dateOfBirth, gender,
					phoneNumber, position, department,
					yearOfEmployment, monthOfEmployment, dayOfEmployment, salary	
			);
		
		EmployeeValidator ev = new EmployeeValidator (newEmp, scan);
		if(ev.validateAll()) {
			for(Department d : acsw.getDepartments()) {
				if(newEmp.getDepartment() == d)	{
					acsw.addEmployee(newEmp, d);
					System.out.println();
					System.out.println("New Employee successfully added:");
					System.out.println(newEmp);
					if(newEmp.getSalary() > acsw.getHighestPay()) {
						acsw.swapHighestPay(newEmp);
					}
					if(newEmp.getYearOfEmployment() < acsw.getFirstYear()) {
						acsw.swapFirstYear(newEmp);
					}
					break;
				}
			}
		} else {
			System.out.println("Unable to add the employee");
		}
		
	}
		
	//removes the selected employee from the system
	public void fireEmployee() {
		System.out.println("Select department: ");
		Department department = null;
				
		Department[] dpts = new Department[acsw.getDepartments().size()];
		acsw.getDepartments().toArray(dpts);
		for(int i = 0; i < dpts.length; i++) {
			System.out.println((i + 1) + " - " + dpts[i].getName());
		}
		System.out.print("Enter selection: ");
		if(scan.hasNextInt()) {
			int selectDpt = Integer.parseInt(scan.nextLine());
			if((selectDpt > 0) && (selectDpt <= dpts.length)) {
				department = dpts[selectDpt-1];
			} else {
				System.out.println("Invalid choice!");
				return;
			}
		}
		if(department.getEmployees().size() == 0) {
			System.out.println("This department is empty.");
			System.out.println();
		} else {
			Employee[] emps = new Employee[department.getEmployees().size()];
			emps = department.getEmployees().toArray(emps);
			System.out.println("These employees work at "+department.getName()+":");
			for(int i = 0; i < emps.length; i++) {
				System.out.println(i+1 + " " + emps[i].getFullName());
			}
			System.out.print("Select an employee: ");
			int choice = 0;
			if(scan.hasNextInt()) {
				choice = scan.nextInt();
				if((choice > 0)&& (choice <= emps.length)) {
					acsw.getEmployees().remove(choice-1);
					department.getEmployees().remove(choice-1);
					System.out.println("The employee is no longer registered in the system.");
					System.out.println();
				} else {
					System.out.println("Wrong input!");
					System.out.println();
					return;
				}
			} else {
				System.out.println("Wrong input!");
				return;
			}
		}
	}
	
	//allows to change information about an employee
	public void changeInfo() {
		System.out.println("Select department: ");
		Department department = null;
		Department[] dpts = new Department[acsw.getDepartments().size()];
		dpts = acsw.getDepartments().toArray(dpts);
		for(int i = 0; i < dpts.length; i++) {
			System.out.println((i + 1) + " - " + dpts[i].getName());
		}
		System.out.print("Enter selection: ");
		if(scan.hasNextInt()) {
			int selectDpt = Integer.parseInt(scan.nextLine());
			if((selectDpt > 0) && (selectDpt <= dpts.length)) {
				department = dpts[selectDpt-1];
			} else {
				System.out.println("Invalid choice!");
				return;
			}
		}
		if(department.getEmployees().size() == 0) {
			System.out.println("This department is empty.");
		} else {
			Employee[] emps = new Employee[department.getEmployees().size()];
			emps = department.getEmployees().toArray(emps);
			System.out.println("These employees work at "+department.getName()+":");
			for(int i = 0; i < emps.length; i++) {
				System.out.println(i+1 + " " + emps[i].getFullName());
			}
			System.out.print("Select an employee: ");
			if(scan.hasNextInt()) {
				int choice = Integer.parseInt(scan.nextLine());
				if((choice > 0)&& (choice <= emps.length)) {
					modifyEmployee(emps[choice-1]);
				} else {
					System.out.println("Wrong input!");
					return;
				}
			} else {
				System.out.println("Wrong input!");
				return;
			}
		}
	}
	
	//enables to change the info about selected employee's name, position etc.
	public void modifyEmployee(Employee e) {
		System.out.println("Viewing data about: ");
		System.out.println(e);
		System.out.println("---------------");
		EmployeeValidator evLocal = new EmployeeValidator(e, scan);
		
		System.out.println("What do you want to change?");
		System.out.println("1 - first name");
		System.out.println("2 - last name");
		System.out.println("3 - phone number");
		System.out.println("4 - position");
		System.out.println("5 - department");
		System.out.println("6 - superior");
		System.out.println("7 - salary");
		System.out.print("Select here: ");
		if(scan.hasNextInt()) {
			int choice = Integer.parseInt(scan.nextLine());
			if(choice < 1 && choice > 7) {
				System.out.println("Wrong input!");
				return;
			} else {
				switch(choice) {
					case 1:
						System.out.print("Enter name: ");
						String newName = scan.nextLine();
						if(evLocal.validateFirstName())	{
							e.setFirstName(newName);
							System.out.println("Name changed to "+newName);
						} else {
							System.out.println("Unable to change the employee's name.");
						}
						break;
					
					case 2:
						System.out.print("Enter surname: ");
						String newLastName = scan.nextLine().trim();
						if(evLocal.validateLastName()) {
							e.setFirstName(newLastName);
							System.out.println("Name changed to "+newLastName);
						}
						else {
							System.out.println("Unable to change the employee's last name.");
						}
						break;
						
					case 3:
						System.out.print("Enter phone number: ");
						String newPhoneNumber = scan.nextLine().trim();
						if(evLocal.validatePhoneNumber()) {
							e.setPhoneNumber(newPhoneNumber);
							System.out.println("Employee's phone number changed to "+newPhoneNumber);
						} else {
							System.out.println("Unable to change the employee's phone number.");
						}
						break;
						
					case 4:
						System.out.print("Enter the employee's new position: ");
						String newPosition = scan.nextLine().trim();
						e.setPosition(newPosition);
						System.out.println(e.getFullName() + "'s new position is "+ newPosition+".");
						System.out.println();
						break;
						
					case 5:
						System.out.println("Select department: ");
						Department department = null;
						Department[] dpts = new Department[acsw.getDepartments().size()];
						dpts = acsw.getDepartments().toArray(dpts);
						for(int i = 0; i < dpts.length; i++) {
							System.out.println((i + 1) + " - " + dpts[i].getName());
						}
						System.out.print("Enter selection: ");
						if(scan.hasNextInt()) {
							int selectDpt = Integer.parseInt(scan.nextLine());
							if((selectDpt > 0) && (selectDpt <= dpts.length)) {
								e.setDepartment(department);
								System.out.println(e.getFullName() + " now belongs to this department: "+e.getDepartment().getName());
								System.out.println();
							} else {
								System.out.println("Invalid choice!");
								return;
							}
						}
						break;
						
					case 6:
						System.out.println("Select superior employee: ");
						Employee[] emps = new Employee[e.getDepartment().getEmployees().size()];
						emps = e.getDepartment().getEmployees().toArray(emps);
						
						for(int i = 0; i < emps.length; i++) {
							System.out.println(i+1 + " - "+ emps[i].getFullName()+", "+emps[i].getPosition());
						}
						
						System.out.print("Choose the employee's superior: ");
						if(scan.hasNextInt()) {
							int choiceNewSuperior = Integer.parseInt(scan.nextLine());
							if(choiceNewSuperior > 0 && choiceNewSuperior < emps.length && emps[choiceNewSuperior-1] != e) {
								e.setSuperior(emps[choiceNewSuperior-1]);
								System.out.println(e.getFullName() + "'s superior is now " + e.getSuperior().getFullName());
								System.out.println();
							} else {
								System.out.println("Invalid choice!");
								return;
							}
						} else {
							System.out.println("Invalid choice!");
							return;
						}
						break;
						
					case 7:
						System.out.print("Enter the employee's new salary: ");
						if(scan.hasNextInt()) {
							e.setSalary(Integer.parseInt(scan.nextLine()));
							System.out.println(e.getFullName() + "'s salary has been set to " + e.getSalary());
							System.out.println();
						} else {
							System.out.println("Wrong input.");
							return;
						}
						break;
				}
			}
		}
		else {
			System.out.println("Wrong input!");
			return;
		}
	}
	
}
