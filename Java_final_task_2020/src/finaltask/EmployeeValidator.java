package finaltask;

import java.util.Scanner;

public class EmployeeValidator {
	private Employee employee;
	@SuppressWarnings("unused")
	private Scanner scan;
	
	public EmployeeValidator(Employee employee, Scanner scan) {
		this.employee = employee;
		this.scan = scan;
	}
	
	public boolean validateAll() {
		return validateFullName() && validateDates() && validateGender() && validatePhoneNumber();
	}
	
	private boolean validateFullName() {
		String regex = "[A-Z]{1}[a-zA-Z]+";
		
		return ((employee.getFirstName().matches(regex)) && (employee.getLastName().matches(regex)));
	}
	
	public boolean validateFirstName() {
		String regex = "[A-Z]{1}[a-zA-Z]+";
		
		return (employee.getFirstName().matches(regex));
	}
	
	public boolean validateLastName() {
		String regex = "[A-Z]{1}[a-zA-Z]+";
		
		return (employee.getLastName().matches(regex));
	}
	
	public boolean validatePhoneNumber() {
		String regex = "[\\d -]+";
		return employee.getPhoneNumber().matches(regex);
	}
	
	private boolean validateDates() {
		String regex = "\\d{4}[ -.]{1}\\d{1,2}[ -]{1}\\d{1,2}";
		
		return( (employee.getDateOfBirth().matches(regex)) && ((employee.getDateOfEmployment().matches(regex))) );
	}
	
	private boolean validateGender() {
		return(
				employee.getGender().equals("male")
				|| employee.getGender().equals("female")
				);
	}
}
