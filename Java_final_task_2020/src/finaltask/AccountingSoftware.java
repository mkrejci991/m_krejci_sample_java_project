package finaltask;

import java.io.*;
import java.util.*;

@SuppressWarnings("serial")
public class AccountingSoftware implements Serializable {
	private ArrayList<Employee> employees;
	private ArrayList<Department> departments;
	@SuppressWarnings("unused")
	private Scanner scan;
	@SuppressWarnings("unused")
	private File fileEmps;
	@SuppressWarnings("unused")
	private File fileDpts;
	//salary of the most expensive Employee
	private int highestPay = 0;
	//the earliest year somebody was employed
	private int firstYear = 0;
	
	
	@SuppressWarnings("unchecked")
	public AccountingSoftware(Scanner scan) {
		String fileDptsName = System.getProperty("user.dir")+File.separator+"Departments.txt";
		String fileEmpsName = System.getProperty("user.dir")+File.separator+"Employees.txt";
		
		//for the departments
		fileDpts = new File(fileDptsName);
		if(!fileDpts.exists()) {
			departments = new ArrayList<Department>();
			
			try	{
				if(fileDpts.createNewFile()) {
					try(	FileOutputStream fos = new FileOutputStream(fileDpts);
							ObjectOutputStream oos = new ObjectOutputStream(fos) ) {
						//An OBJECT must be written here! therefore the departments List
						oos.writeObject(departments);
					}
					catch(Exception e) {
						System.err.println("An exception occured!");
						return;
					}
				} else {
					System.out.println("Something went wrong!");
				}
			} catch(IOException ex)	{
				System.err.println("An exception occured!");
				ex.printStackTrace();
				return;
			}
		}
		
		else {
			try(FileInputStream fis = new FileInputStream(fileDpts);
					ObjectInputStream ois = new ObjectInputStream(fis))	{
				departments = (ArrayList<Department>)ois.readObject();
			} catch(Exception ex) {
				System.err.println("An exception occured.");
				ex.printStackTrace();
				return;
			}
		}
		
		//for the employees
		fileEmps = new File(fileEmpsName);
				
				if(!fileEmps.exists()) {
					employees = new ArrayList<Employee>();
					try	{
						if(fileEmps.createNewFile()) {
							try(	FileOutputStream fos = new FileOutputStream(fileEmps);
									ObjectOutputStream oos = new ObjectOutputStream(fos) ) {
								oos.writeObject(employees);
							} catch(Exception e) {
								System.err.println("An exception occured!");
								return;
							}
						} else {
							System.out.println("Something went wrong!");
						}
					} catch(IOException ex)	{
						System.err.println("An exception occured!");
						ex.printStackTrace();
						return;
					}
				}
				else {
					try(FileInputStream fis = new FileInputStream(fileEmps);
							ObjectInputStream ois = new ObjectInputStream(fis))	{
						employees = (ArrayList<Employee>)ois.readObject();
					} catch(Exception ex) {
						System.err.println("An exception occured.");
						ex.printStackTrace();
						return;
					}
				}
			this.scan = scan;
	}
	
	public ArrayList<Employee> getEmployees() {
		return employees;
	}
	
	public ArrayList<Department> getDepartments() {
		return departments;
	}
	
	public int getHighestPay() {
		return highestPay;
	}
	
	public int getFirstYear() {
		return firstYear;
	}
	
	public boolean addEmployee(Employee e, Department d) {
		if( (!employees.contains(e)) && (!d.getEmployees().contains(e)) ) {
			employees.add(e);
			d.getEmployees().add(e);
			return true;
		}
		
		return false;
	}
	
	//the Accounting software's main menu method
	public void runAccSoftware() {
		System.out.println("== The Accounting Software(v. 0.9) ==");
		while(true) {
			System.out.println("============================ MAIN MENU ===============================");
			System.out.println("Chose an operation: ");
			System.out.println("1 - Recruit a new employee");
			System.out.println("2 - Fire an Employee");
			System.out.println("3 - Change an employee's information");
			System.out.println("4 - View info about departments / employees");
			System.out.println("5 - Exit program");
			System.out.print("Enter choice: ");
			String choice = scan.nextLine();
			EmployeeManager em = new EmployeeManager(this, scan);
			switch(choice) {
				case "1":
					System.out.println();
					System.out.println("** Recruit new employees menu **");
					em.addEmployee();
					System.out.println();
					break;
					
				case "2":
					System.out.println();
					System.out.println("** Fire employees menu **");
					em.fireEmployee();
					System.out.println();
					break;
					
				case "3":
					System.out.println();
					System.out.println("** Change information about employees **");
					em.changeInfo();
					System.out.println();
					break;
				
				case "4":
					System.out.println("** Viewing info about departments / employees **");
					viewAboutDepartments();
					System.out.println();
					break;
					
				case "5":
					closeAccSoftware();
					return;
					
				default:
					System.out.println("Invalid choice");
					break;
				
			}
		}
	}
	
	// point 4 of the Main menu method runAccSoftware()
	private void viewAboutDepartments() {
		System.out.println("What do you want to view?");
		System.out.println("1 - Search for employees");
		System.out.println("2 - View info about departments / add a new department");
		System.out.println("3 - view average salary");
		System.out.println("4 - view top 10 most expensive employees");
		System.out.println("5 - view top 10 most loyal employees");
		System.out.println("6 - Set an employee as the head of a department");
		System.out.println("BACK - return to previous menu");
		
		System.out.print("Enter selection: ");
		String choice = scan.nextLine().toLowerCase().trim();
		
		switch(choice) {
		
			case "1":
				searchForEmployees();
				System.out.println();
				break;
			
			case "2":
				System.out.println("Viewing info about departments / add a new department");
				showDepartmentInfo();
				System.out.println();
				break;
				
			case "3":
				System.out.println("Average salary");
				System.out.println("For the whole company: " + calculateAverageSalaryForAllEmployees());
				for (Department d : departments)
				{
					System.out.println(d.getName() + ": " + d.calculateAverage());
				}
				System.out.println();
				break;
			
			case "4":
				System.out.println("Viewing top 10 most expensive employees");
				printTopTenExpensiveEmployees(employees);
				System.out.println();
				break;
			
			case "5":
				System.out.println("Viewing top 10 most loyal employees");
				printTopTenLoyalEmployees(employees);
				System.out.println();
				break;
				
			case "6":
				setHeadOfDepartment();
				System.out.println();
				break;
				
			case "back":
				System.out.println();
				return;
		
			default:
				System.out.println("Invalid choice");
				System.out.println();
				return;
		}
	}
	
	//sets the head of a chosen department
	public void setHeadOfDepartment() {
		System.out.println("Select department: ");
		Department department = null;
		Department[] dpts = new Department[departments.size()];
		dpts = departments.toArray(dpts);
			for(int i = 0; i < dpts.length; i++) {
				System.out.println(dpts[i].getName() + " - " + (i + 1));
			}
		System.out.print("Enter selection: ");
		int selectDpt = Integer.parseInt(scan.nextLine());
		department = dpts[selectDpt-1];
		System.out.println();
		
		System.out.println("Employees of " + department.getName() + "department:");
		Employee[] emps = new Employee[department.getEmployees().size()];
		emps = department.getEmployees().toArray(emps);
		
		for(int i = 0; i < emps.length; i++) {
			System.out.println(i+1 + " - "+ emps[i].getFullName()+", "+emps[i].getPosition());
		}
		
		System.out.print("Choose the head of the department: ");
		if(scan.hasNextInt()) {
			int choiceNewHead = Integer.parseInt(scan.nextLine());
			if(choiceNewHead > 0 && choiceNewHead < emps.length) {
				
				if(department.getHeadOfDpt() != null) {
					System.out.printf("Current head of this department is %s, do you wish to replace them? [yes/no]\n", department.getHeadOfDpt());
					String yesNoChoice = scan.nextLine().toLowerCase().trim();
					if(yesNoChoice.contentEquals("yes")) {
						department.setHeadOfDpt(emps[choiceNewHead-1]);
						System.out.printf("%s is now the head of %s department\n", department.getHeadOfDpt().getFullName(), department.getName());
						System.out.println();
					} else {
						return;
					}
				} else {
					department.setHeadOfDpt(emps[choiceNewHead-1]);
					System.out.printf("%s is now the head of %s department\n", department.getHeadOfDpt().getFullName(), department.getName());
					System.out.println();
				}
			} else {
				System.out.println("Invalid choice!");
				return;
			}
		}
		System.out.println();
		
	}
	
	
	//calculates the average salary for all employees
	public double calculateAverageSalaryForAllEmployees() {
		double avgSalary = 0;
		
		for(Employee e : employees)	{
			avgSalary += e.getSalary();
		}
		avgSalary /= employees.size();
		
		return avgSalary;
	}
	
	public void swapHighestPay(Employee e) {
		this.highestPay = e.getSalary();
	}
	
	public void swapFirstYear(Employee e) {
		this.firstYear = e.getYearOfEmployment();
	}
	
	//searching for employees menu, point 1 of viewAboutDepartments() method
	private void searchForEmployees() {
		System.out.println("Searching for employees by...");
		System.out.println("... name - 1");
		System.out.println("... position - 2");
		System.out.println("... department - 3");
		System.out.println("... superior - 4");
		System.out.print("Enter choice: ");
		String choice = scan.nextLine();
		
		
		switch(choice) {
			case "1":
				searchByName();
				break;
			
			case "2":
				searchByPosition();
				break;
				
			case "3":
				searchByDepartment();
				break;
			
			case "4":
				searchBySuperior();
				break;
				
			default:
				System.out.println("Invalid choice!");
				return;
		}
		
	}
	
	// searches for employees by name in the searchForEmployees() method
	private void searchByName()	{
		ArrayList<Employee> foundEmps = new ArrayList <Employee>();
		System.out.print("Enter name: ");
		String nameInput = scan.nextLine();
		for (Employee e : employees) {
			if (e.getFirstName().equals(nameInput) || e.getLastName().equals(nameInput) || e.getFullName().equals(nameInput)) {
				foundEmps.add(e);
			}
		}
		if(foundEmps.size() == 0) {
			System.out.println("No employees with this name found");
		} else {
			System.out.println("Employees with name containing " + nameInput + ":");
			for (Employee e : employees) {
				System.out.printf("%s, %s in %s\n", e.getFullName(), e.getPosition(), e.getDepartment());
			}
		}
	}
	
	//searching by position in the searchForEmployees() method
	private void searchByPosition() {
		ArrayList<Employee> foundEmps = new ArrayList <Employee>();
		System.out.print("Enter position: ");
		String posInput = scan.nextLine();
		for (Employee e : employees) {
			if (e.getPosition().contentEquals(posInput)) {
				foundEmps.add(e);
			}
		}
		if(foundEmps.size() == 0) {
			System.out.println("No employees on this position found");
		} else {
			System.out.println("Employees on position " + posInput + ":");
			for (Employee e : employees) {
				System.out.printf("%s, in %s\n", e.getFullName(), e.getDepartment());
			}
		}
	}
	
	//searching by position in the searchForEmployees() method
	private void searchByDepartment() {
		ArrayList<Employee> foundEmps = new ArrayList <Employee>();
		System.out.print("Enter department: ");
		String dptInput = scan.nextLine();
		for (Employee e : employees) {
			if (e.getDepartment().getName().equalsIgnoreCase(dptInput)) {
				foundEmps.add(e);
			}
		}
		if(foundEmps.size() == 0) {
			System.out.println("No employees working in this department found.");
		}
		else {
			System.out.println("Employees in department of " + dptInput + ":");
			for (Employee e : employees) {
				System.out.printf("%s, in %s\n", e.getFullName(), e.getPosition());
			}
		}
	}
		
	// searching by superior in the searchForEmployees() method	
	private void searchBySuperior() {
		ArrayList<Employee> foundEmps = new ArrayList <Employee>();
		System.out.print("Enter first name, last name or full name of superior employee: ");
		String nameInput = scan.nextLine();
		for (Employee e : employees) {
			if (e.getSuperior().getFirstName().equals(nameInput)
					|| e.getSuperior().getLastName().equals(nameInput) 
					|| e.getSuperior().getFullName().equals(nameInput))	{
				foundEmps.add(e);
			}
		} if(foundEmps.size() == 0) {
			System.out.println("No employees with this name found");
		}
		else {
			System.out.println("Employees under " + nameInput + ":");
			for (Employee e : employees) {
				System.out.printf("%s, %s in %s\n", e.getFullName(), e.getPosition(), e.getDepartment());
			}
		}
	}	
		
	
	//allows to search for the information about departments
	public void showDepartmentInfo() {
		System.out.println("Select department: ");
		Department department = null;
				
		Department[] dpts = new Department[getDepartments().size()];
		getDepartments().toArray(dpts);
		for(int i = 0; i < dpts.length; i++) {
			System.out.println((i + 1) + " - " + dpts[i].getName());
		}
		System.out.println("... or add a new department (type in NEW): ");
		System.out.print("Enter selection: ");
		String dptSelection = scan.nextLine().trim().toLowerCase();
		
		if(dptSelection.equals("new")) {
			System.out.print("Enter the name of the new department: ");
			String newDptName = scan.nextLine();
			Department newDpt = new Department(newDptName);
			departments.add(newDpt);
			System.out.println("New department successfully added!");
		} else {
			int selectDpt = Integer.parseInt(dptSelection);
					
				if((selectDpt > 0) && (selectDpt <= dpts.length)) {
					department = dpts[selectDpt-1];
				} else {
					System.out.println("Invalid choice!");
					return;
				}
					
				System.out.println(department.getName());
				if(department.getHeadOfDpt() == null) {
					System.out.println("This department does not have an established head employee.");
				} else {
					System.out.println("Head of department: " + department.getHeadOfDpt().getFullName());
				}
				
				System.out.println("Employees working here:");
				for(Employee e : department.getEmployees())	{
					System.out.println(e.getFullName());
					System.out.println("Position: " + e.getPosition());
					System.out.println();
				}
		}
		
	}
		
	//prints top 10 most expensive employees
	public void printTopTenExpensiveEmployees(ArrayList<Employee> employees) {
		int maxLength = 0;
		if (employees.size() < 10) {
			maxLength = employees.size();
		} else {
			maxLength = 10;
		}
				
		int counter = 1;
		while(counter <= maxLength)	{
			for (int i = highestPay; i > 0; i--) {
				for (Employee e : employees) {
					if(e.getSalary() == i) {
						System.out.printf("%d. %s, %s (salary: %d)\n",counter, e.getFullName(), e.getDepartment().getName(), i);
						counter ++;
					}
				}
			}
		}
	}
	
	//prints top 10 loyal employees
	public void printTopTenLoyalEmployees(ArrayList<Employee> employees) {
		int maxLength = 0;
		if (employees.size() < 10) {
			maxLength = employees.size();
		} else {
			maxLength = 10;
		}
				
		int counter = 1;
		while(counter <= maxLength)	{
			for (int i = firstYear; i <= 2020; i++) {
				for (Employee e : employees) {
					if(e.getYearOfEmployment() == i) {
						System.out.printf("%d. %s, %s (employed since %d)\n",counter, e.getFullName(), e.getDepartment().getName() ,e.getYearOfEmployment());
						counter ++;
					}
				}
			}
		}
	}
	
	// Writes the files, closes the program
	public void closeAccSoftware() {
		//for departments
		try(	FileOutputStream fos = new FileOutputStream(fileDpts);
				ObjectOutputStream oos = new ObjectOutputStream(fos)) {
			oos.writeObject(departments);
			
		} catch(Exception e) {
			System.err.println("An exception occured!");
			e.printStackTrace();
		}
		
		//for employees
		try(	FileOutputStream fos = new FileOutputStream(fileEmps);
				ObjectOutputStream oos = new ObjectOutputStream(fos)) {
			oos.writeObject(employees);
			
		}
		catch(Exception e) {
			System.err.println("An exception occured!");
			e.printStackTrace();
		}
		
		System.out.println("Accounting software successfully closed.");
	}
	
}
